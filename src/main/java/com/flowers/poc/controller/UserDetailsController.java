package com.flowers.poc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flowers.poc.entity.UserDetails;
import com.flowers.poc.response.BaseResponse;
import com.flowers.poc.service.UserDetailsService;

@RestController
@RequestMapping(value = "/api/v1")
public class UserDetailsController {

	@Autowired
	private UserDetailsService userDetailsService;
	

	/* Create REST endpoints that reads the above JSON feed using HTTP. */
	@GetMapping(value = "/readjson")
	public ResponseEntity<BaseResponse<List<UserDetails>>> readJson() {

		BaseResponse<List<UserDetails>> response = new BaseResponse<List<UserDetails>>();

		List<UserDetails> userDetails = userDetailsService.getUserDetails();

		if (userDetails != null) {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("Details Found Suceesfully");
		} else {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMessage("Details Not Found");
		}
		return new ResponseEntity<BaseResponse<List<UserDetails>>>(response, HttpStatus.OK);

	}

	/* Count endpoint */
	/*tried to hit actuator Mappings ID to get the list of endpoints */
	@GetMapping(value = "/countendpoint")
	public ResponseEntity<BaseResponse<String>> countEndPoint() {

		BaseResponse<String> response = new BaseResponse<String>();

		int count = userDetailsService.getCount();

		response.setData("count is : " + String.valueOf(count));
		response.setStatusCode(HttpStatus.OK.value());
		response.setMessage("Details Found Suceesfully"); 
		
		return new ResponseEntity<BaseResponse<String>>(response, HttpStatus.OK);

	}

	/*
	 * Tally the number of unique user Ids in the JSON and return in a JSON
	 * response.
	 */
	@GetMapping(value = "/uniqueuser")
	public ResponseEntity<BaseResponse<List<UserDetails>>> uniqueUsers() {

		BaseResponse<List<UserDetails>> response = new BaseResponse<List<UserDetails>>();

		List<UserDetails> userDetails = userDetailsService.getUniqueUser();

		if (userDetails != null) {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("Unique Details Found Suceesfully");
		} else {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMessage("Details Not Found");
		}
		return new ResponseEntity<BaseResponse<List<UserDetails>>>(response, HttpStatus.OK);

	}

	/* Updated User List endpoint */
	@GetMapping(value = "/updateduserlist")
	public ResponseEntity<BaseResponse<List<UserDetails>>> updateUserList() {

		BaseResponse<List<UserDetails>> response = new BaseResponse<List<UserDetails>>();

		List<UserDetails> userDetails = userDetailsService.getUserDetails();

		if (userDetails != null) {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("Details Found Suceesfully.");
		} else {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMessage("Details Not Found.");
		}
		return new ResponseEntity<BaseResponse<List<UserDetails>>>(response, HttpStatus.OK);

	}

	/*
	 * Modify the 4th JSON array item, changing the title and body of the object to
	 * "1800Flowers"
	 */
	@PatchMapping(value = "/modifyuserdetails")
	public ResponseEntity<BaseResponse<UserDetails>> modifyUserDetails(@RequestParam int id, @RequestParam String title,
			@RequestParam String body) {

		BaseResponse<UserDetails> response = new BaseResponse<UserDetails>();

		UserDetails userDetails = userDetailsService.updateUserDetails(id, title, body);

		if (userDetails != null) {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("Details Modified Suceesfully.");
		} else {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMessage("User not found.");
		}
		return new ResponseEntity<BaseResponse<UserDetails>>(response, HttpStatus.OK);

	}

	/* Return the modified JSON in the endpoint response. */
	@GetMapping(value = "/modifieduserdetails")
	public ResponseEntity<BaseResponse<UserDetails>> modifiedUserDetails() {

		int id = 4;
		String title = "1800Flowers";
		String body = "1800Flowers";

		BaseResponse<UserDetails> response = new BaseResponse<UserDetails>();

		UserDetails userDetails = userDetailsService.updateUserDetails(id, title, body);

		if (userDetails != null) {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("Details Found Suceesfully");
		} else {
			response.setData(userDetails);
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMessage("Details Not Found");
		}
		return new ResponseEntity<BaseResponse<UserDetails>>(response, HttpStatus.OK);

	}

}
