package com.flowers.poc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flowers.poc.entity.UserDetails;
import com.flowers.poc.service.UserDetailsService;

@SpringBootApplication
public class Application implements CommandLineRunner{
	
	@Autowired
	private UserDetailsService userDetailsService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		int id = 4; 
		String title = "1800Flowers";
		String body = "1800Flowers";
		
		UserDetails userDetails = userDetailsService.updateUserDetails(id, title, body);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(userDetails);

		System.out.println("Modified Json response : " + jsonData);
	}

}
