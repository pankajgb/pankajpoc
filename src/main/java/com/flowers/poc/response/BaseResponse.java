package com.flowers.poc.response;

import org.springframework.http.HttpStatus;

public class BaseResponse<T> {
	
	T data;
	
	int statusCode;
	
	String message;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}  

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	} 
}
