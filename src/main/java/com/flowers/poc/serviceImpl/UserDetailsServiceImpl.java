package com.flowers.poc.serviceImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.flowers.poc.controller.UserDetailsController;
import com.flowers.poc.entity.UserDetails;
import com.flowers.poc.service.UserDetailsService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	Logger logger = LoggerFactory.getLogger(UserDetailsController.class);

	@Autowired
	private RestTemplate restTemplate;

	@Value("${userdetails.url}")
	private String userDetailsUrl;
	
	@Value("${endpoints.url}")
	private String endPointsUrl;
	

	@Override
	public List<UserDetails> getUserDetails() {
		// TODO Auto-generated method stub

		UserDetails[] UserDetails = restTemplate.getForObject(userDetailsUrl, UserDetails[].class);

		return Arrays.asList(UserDetails);
	}

	@Override
	public List<UserDetails> getUniqueUser() {
		// TODO Auto-generated method stub
		List<UserDetails> userDetails = getUserDetails();

		return userDetails.stream().filter(findUnique(user -> user.getUserId())).collect(Collectors.toList());

	}

	/* predicate to find unique result */
	public <T> Predicate<T> findUnique(Function<? super T, ?> key) {
		Map<Object, Boolean> chk = new ConcurrentHashMap<>();
		return t -> chk.putIfAbsent(key.apply(t), Boolean.TRUE) == null;
	} 

	@Override
	public UserDetails updateUserDetails(int id, String title, String body) {
		// TODO Auto-generated method stub
		List<UserDetails> userDetails = getUserDetails();
		UserDetails userDetail = userDetails.stream().filter(user -> user.getId() == 4).findAny().orElse(null);
		userDetail.setTitle(title);
		userDetail.setBody(title);
		return userDetail;
	}

	/*tried to hit actuator Mappings ID to get the list of endpoints */
	@Override
	public int getCount() {
		String endpoints = restTemplate.getForObject(endPointsUrl, String.class);
		System.out.println(endpoints); /*unable to extract endpoint from string*/
		return 0;
	}

	/*@Override
	public int getCount() {
		// TODO Auto-generated method stub
		List<UserDetails> userDetails = getUserDetails();
		
		return userDetails.size();
	}*/
	
	

}
