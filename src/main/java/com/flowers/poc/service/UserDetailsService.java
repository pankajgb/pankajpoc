package com.flowers.poc.service;

import java.util.List;

import com.flowers.poc.entity.UserDetails;

public interface UserDetailsService {
	
	public List<UserDetails> getUserDetails();
	
	public int getCount();
	
	public List<UserDetails> getUniqueUser();
	
	public UserDetails updateUserDetails(int id, String title, String body);
	

}
