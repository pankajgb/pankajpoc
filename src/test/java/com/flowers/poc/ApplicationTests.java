package com.flowers.poc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.flowers.poc.entity.UserDetails;
import com.flowers.poc.service.UserDetailsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@SpringBootTest
class ApplicationTests {
 
	
	@Mock
	UserDetailsService userDetailsService;

	@Test
	public void testgetUserDetails() {

		List<UserDetails> userDetails = new ArrayList<UserDetails>();

		userDetails.add(new UserDetails(101, 201, "title1", "body1"));
		userDetails.add(new UserDetails(102, 201, "title2", "body2"));
		userDetails.add(new UserDetails(103, 201, "title3", "body3"));

		when(userDetailsService.getUserDetails()).thenReturn(userDetails);
		
		assertEquals(3, userDetailsService.getUserDetails().size());

	}

	/*@Test
	public void testUserCount() {

		List<UserDetails> userDetails = new ArrayList<UserDetails>();
		userDetails.add(new UserDetails(101, 201, "title1", "body1"));
		userDetails.add(new UserDetails(102, 201, "title2", "body2"));
		userDetails.add(new UserDetails(103, 201, "title3", "body3"));
		
		when(userDetailsService.getCount()).thenReturn(userDetails.size());
		
		assertEquals(3, userDetailsService.getCount());

	}*/

	@Test
	public void testUniqueUsers() {

		List<UserDetails> users = new ArrayList<UserDetails>();
		users.add(new UserDetails(101, 201, "title1", "body1"));
		users.add(new UserDetails(102, 202, "title2", "body2"));
		users.add(new UserDetails(101, 201, "title3", "body3"));

		List<UserDetails> uniqueUsers = new ArrayList<UserDetails>();
		uniqueUsers.add(new UserDetails(101, 201, "title1", "body1"));
		uniqueUsers.add(new UserDetails(102, 202, "title2", "body2"));

		when(userDetailsService.getUniqueUser()).thenReturn(uniqueUsers);
		
		assertEquals(2, userDetailsService.getUniqueUser().size());
	}

	@Test
	public void updateUserDetails() {
		List<UserDetails> userDetails = new ArrayList<UserDetails>();
		userDetails.add(new UserDetails(101, 201, "title1", "body1"));
		userDetails.add(new UserDetails(102, 201, "title2", "body2"));
		userDetails.add(new UserDetails(103, 201, "title3", "body3"));

		UserDetails updatedUser = new UserDetails(103, 201, "1800Flowers", "1800Flowers");

		when(userDetailsService.updateUserDetails(103, "1800Flowers", "1800Flowers")).thenReturn(updatedUser);
		
		assertThat(updatedUser.getTitle().equals("1800Flowers"));
	}

}
